package kz.azatserzhanov.rx

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    private val arr = mutableListOf(0, 1, 2, 3, 4, 5, 6, 7)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener {
            task1()
            task2()
        }

        Glide
            .with(this)
            .load("https://astana.zakazbuketov.kz/upload/iblock/129/129028ae36d7502e10fd57f95ad954c3.jpg")
            .placeholder(R.drawable.ic_launcher_background)
            .into(bgImageView)
    }

    private fun task1() {
        Observable
            .fromCallable {
                val arr2 = mutableListOf<Int>()
                arr.forEachIndexed { index, elem ->
                    arr2.add(elem + index * 1234)
                    arr2.add(1234 * 124 / 4 * 765)
                    arr2.add(1234 * 124 / 4 * 765)
                    arr2.add(1234 * 124 / 4 * 765)
                    arr2.add(1234 * 124 / 4 * 765)
                    arr2.add(1234 * 124 / 4 * 765)
                }
                arr2
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { listArr2 ->
                    textView.text = listArr2.toString()
                },
                {
                    Log.d("Error", it.message)
                }
            )
    }

    private fun task2() {
        Observable
            .fromCallable {
                val arr3 = mutableListOf<Int>()
                arr.forEachIndexed { index, elem ->
                    arr3.add(elem + index * 4321)
                    arr3.add(1234 * 124 / 4 * 765)
                    arr3.add(1234 * 124 / 4 * 765)
                    arr3.add(1234 * 124 / 4 * 765)
                    arr3.add(1234 * 124 / 4 * 765)
                    arr3.add(1234 * 124 / 4 * 765)
                }
                arr3
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { listArr2 ->
                    textView2.text = listArr2.toString()
                },
                {
                    Log.d("Error", it.message)
                }
            )
    }
}
